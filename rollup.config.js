import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import babel from '@rollup/plugin-babel';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import json from '@rollup/plugin-json';
import typescript from '@rollup/plugin-typescript';
import replace from '@rollup/plugin-replace';
import { terser } from 'rollup-plugin-terser';
import html2 from "rollup-plugin-html2";
import svelte from "rollup-plugin-svelte";
import postcss from "rollup-plugin-postcss";
import sveltePreprocessor from "svelte-preprocess";
import livereload from 'rollup-plugin-livereload'



const dev = process.argv.indexOf('-w') > -1;

const pluginsServer = [
    peerDepsExternal(),
    typescript(),
    replace(({
        'process.env.NODE_ENV': JSON.stringify(dev ? 'development' : 'production')
    })),
    babel({
        "babelHelpers": 'runtime',
        "exclude": ["node_modules/**", "./client/**"],
        "presets": ["@babel/env", "@babel/preset-react"],
        "plugins": [
            "@babel/plugin-proposal-class-properties",
            "@babel/plugin-transform-runtime"
        ]
    }),
    json(),
    resolve(),
    commonjs()
];

const pluginsClient = [
    resolve({ browser:true } ),
    commonjs({ include: "node_modules/**", extensions: [".js", ".ts"] }),
    svelte({
        dev,
        extensions: [".svelte"],
        preprocess: sveltePreprocessor(),
        emitCss: true
    }),
    postcss({
        extract: true,
    }),
    typescript(),
    html2({
        template: "./client/index.html",
        fileName: "index.html",
        onlinePath: '/static'
    }),
];

if (!dev) {
    pluginsServer.push(terser()); // minify generated es bundle
    pluginsClient.push(terser()); // minify generated es bundle
} else {
    pluginsClient.push( livereload({ watch: "./dist/public" }) )
}

const server = {
    input: './src/server.ts',
    output: {
        file: 'dist/server.js',
        format: 'cjs'
    },
    plugins: pluginsServer,
    watch: {
        exclude: ['node_modules/**', "./client/**"]
    }
};

const front = {
    input: './client/src/index.ts',
    output: {
        file: 'dist/public/index.js',
        format: 'cjs'
    },
    plugins: pluginsClient,
    watch: {
        exclude: 'node_modules/**'
    }
};

export default [server, front];
