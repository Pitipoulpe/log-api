const routes = {
    INDEX: '/',
    WATCH: '/watch/:uuid',
    POST: '/log/:uuid'
};

export default routes;
