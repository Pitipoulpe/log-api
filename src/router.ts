import { Request, Response } from "express";
import { NodesController } from "./controllers/nodes.controller";
import routes from './config/routes';

export class Router {
    public nodesController: NodesController = new NodesController();
    public _io = null;

    public io(io):void {
        this._io = io;
        this.nodesController.io(io);
    }

    public routes(app): void {
        app.route(routes.INDEX).get(this.nodesController.index);

        app.route(routes.WATCH).get(this.nodesController.watcher);

        app.route(routes.POST).post(this.nodesController.log);

    }
}
