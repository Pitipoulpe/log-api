import { Request, Response } from 'express';
import { v4 as uuidv4 } from 'uuid';
import path from 'path';
import routes from '../config/routes';
//import {io} from '../server';
import {ChatEvent} from "../constants/global.contants";
import {LogMessageInterface} from "../interfaces/logApp.interfaces";

export class NodesController {
    private _io = null;
    private socket = null;

    public io = (io):void => {
        this._io = io;
        this._io.on(ChatEvent.CONNECT, (socket: any) => {
            this.socket = socket;
            socket.on(ChatEvent.MESSAGE, (m: LogMessageInterface) => {
                console.log('[server](message): %s', JSON.stringify(m));
                this._io.emit('message', m);
            });

            socket.on(ChatEvent.JOIN_ROOM, (roomId) => {
                console.log(roomId);
                socket.join(`room/${roomId}`);
            });

            socket.on(ChatEvent.DISCONNECT, () => {
                console.log('Client disconnected');
            });
        });
    };

    public index = (req: Request, res: Response) => {
        const uuid:string = uuidv4();
        console.log(uuid);
        res.redirect(routes.WATCH.replace(':uuid', uuid));
    };

    public watcher = (req: Request, res: Response) => {
        const uuid = req.params.uuid;
        if (!uuid)
            res.redirect(routes.INDEX);

        res.sendFile(path.join(__dirname + '/public/index.html'))

    };

    public log = (req: Request, res: Response) => {
        const uuid = req.params.uuid;
        if (!uuid)
            res.status(500).send({ error: 'UUID room is required' });

        this._io.to('room/'+uuid).emit('log', {log: req.body});
        res.send({ok:'ok'})
        //this._io.to('room/'+uuid).emit('some event');
    }
}
