import express from "express";
import bodyParser from "body-parser";
//import socketIo from 'socket.io';
import cors from 'cors';
import {createServer, Server} from 'http';
import {Router} from "./router";
import {ChatEvent, PORT} from "./constants/global.contants";
import {LogMessageInterface} from "./interfaces/logApp.interfaces";
const ServerIo = require('socket.io');
class LogServer {
    private readonly port: string | number;
    public _app: express.Application;
    public routePrv: Router = new Router();
    private io;
    private server: Server;

    constructor() {
        this._app = express();
        this.port = process.env.PORT || PORT;
        this.config();
        this.initSocket();
        this.routePrv.routes(this._app);
        this.routePrv.io(this.io);
        this.listen();
    }

    private config(): void {
        this.server = createServer(this._app);
        /*this._app.use( ( req, res, next) => {
            console.log(req.protocol + '://' + req.get('host') + req.originalUrl);
            next();
        });*/
        this._app.use(bodyParser.json());
        this._app.use(bodyParser.urlencoded({extended: false}));
        this._app.use('/static', express.static(__dirname + '/public'));
        this._app.use(cors());
        this._app.options('*', cors());

    }

    private initSocket(): void {
        this.io = new ServerIo(this.server);
    }

    public listen(): void {

        this.server.listen(this.port, () => {
            console.log('🚀 Running server on port %s', this.port);
        });
    }

    get app(): express.Application {
        return this._app;
    }

    get io_instance() {
        return this.io;
    }

}

export default LogServer;
