export enum ChatEvent {
    CONNECT = 'connect',
    DISCONNECT = 'disconnect',
    MESSAGE = 'message',
    JOIN_ROOM = 'join-room'
}

export const PORT:number = 3000;

