import doubleDigit from './doubleDigit';

const dateToTime = (date) => {
    const curr_date = doubleDigit(date.getDate());
    const curr_month = doubleDigit(date.getMonth() + 1); //Months are zero based
    const curr_year = doubleDigit(date.getFullYear());
    const curr_hour = doubleDigit(date.getHours());
    const curr_min = doubleDigit(date.getMinutes());
    const curr_sec = doubleDigit(date.getSeconds());

    return `${curr_date}-${curr_month}-${curr_year} : ${curr_hour}:${curr_min}:${curr_sec}`;
};

export default dateToTime;

