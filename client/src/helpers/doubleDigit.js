const doubleDigit = number => number < 10 ? '0'+number : number;

export default doubleDigit;
